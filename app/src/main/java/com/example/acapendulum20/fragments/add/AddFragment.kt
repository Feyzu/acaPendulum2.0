package com.example.acapendulum20.fragments.add

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.acapendulum20.R
import com.example.acapendulum20.model.User
import com.example.acapendulum20.viewmodel.UserViewmodel
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*
import kotlinx.android.synthetic.main.fragment_start_pendulum.view.*
import java.util.*


class AddFragment : Fragment() {

    private lateinit var mUserViewModel: UserViewmodel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add, container, false)

        mUserViewModel = ViewModelProvider(this).get(UserViewmodel::class.java)

        //dropdown sex

        val sex = resources.getStringArray(R.array.sex)
        val arrayAdapterSex = ArrayAdapter(requireContext(), R.layout.dropdown_item, sex)
        view.chooseSex_et.setAdapter(arrayAdapterSex)

        //dropdown handiness
        val handiness = resources.getStringArray(R.array.usedHand)
        val arrayAdapterHandiness = ArrayAdapter(requireContext(), R.layout.dropdown_item, handiness)
        view.addHandiness_et.setAdapter(arrayAdapterHandiness)

        //Date picker
        view.addBirthday_et.setOnClickListener{
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(requireActivity(), { view, year, monthOfYear, dayOfMonth ->
                addBirthday_et.text = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year) }, year, month, day)
            datePickerDialog.show()
        }

        view.add_btn.setOnClickListener{
            insertDataToDatabase()
        }
        return view
    }

    private fun insertDataToDatabase() {
        val firstName = addFirstName_et.text.toString()
        val lastName = addLastName_et.text.toString()
        val birthday = addBirthday_et.text.toString()
        val sex = chooseSex_et.text.toString()
        val handiness = addHandiness_et.text.toString()

        if(inputCheck(firstName, lastName, sex, handiness, birthday)){
            //Create User Object
            val user = User(UUID.randomUUID().toString(), firstName, lastName, birthday, sex, handiness)
            //Add Data to Database
            mUserViewModel.addUser(user)
            Toast.makeText(requireContext(),"Successfully added!", Toast.LENGTH_LONG).show()
            //Navigate Back
            findNavController().navigate(R.id.action_addFragment_to_listFragment)
        }else{
            Toast.makeText(requireContext(),"Please fill out all fields.", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(firstName: String, lastName: String, sex: String, handiness: String, age: String): Boolean{
        val validated = !TextUtils.isEmpty(firstName) && !TextUtils.isEmpty(lastName) && !TextUtils.isEmpty(sex) && !TextUtils.isEmpty(handiness) && !age.isEmpty()
        return validated
    }
}