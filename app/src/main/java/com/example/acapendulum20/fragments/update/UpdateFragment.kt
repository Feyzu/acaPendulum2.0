package com.example.acapendulum20.fragments.update

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.acapendulum20.R
import com.example.acapendulum20.fragments.list.MeasurementList
import com.example.acapendulum20.model.Measurement
import com.example.acapendulum20.model.User
import com.example.acapendulum20.viewmodel.MeasurementViewmodel
import com.example.acapendulum20.viewmodel.UserViewmodel
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*
import kotlinx.android.synthetic.main.fragment_start_pendulum.*
import kotlinx.android.synthetic.main.fragment_update.*
import kotlinx.android.synthetic.main.fragment_update.view.*
import kotlinx.coroutines.processNextEventInCurrentThread
import java.util.*
import kotlin.system.measureNanoTime

class UpdateFragment : Fragment() {

    private val args by navArgs<UpdateFragmentArgs>()

    private lateinit var mUserViewModel: UserViewmodel
    private lateinit var mMeasurementViewmodel: MeasurementViewmodel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_update, container, false)

        //dropdown sex
        val sex = resources.getStringArray(R.array.sex)
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, sex)
        view.updateSex_et.setAdapter(arrayAdapter)

        //dropdown handiness
        val handiness = resources.getStringArray(R.array.usedHand)
        val arrayAdapterHandiness = ArrayAdapter(requireContext(), R.layout.dropdown_item, handiness)
        view.updateHandiness_et.setAdapter(arrayAdapterHandiness)

        //Date picker
        view.updateBirthday_et.setOnClickListener{
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog = DatePickerDialog(requireActivity(), { view, year, monthOfYear, dayOfMonth ->
                updateBirthday_et.text = (dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year) }, year, month, day)
            datePickerDialog.show()
        }

        mUserViewModel = ViewModelProvider(this).get(UserViewmodel::class.java)
        mMeasurementViewmodel = ViewModelProvider(this).get(MeasurementViewmodel::class.java)
        mMeasurementViewmodel.readAllDataForUserId(args.currentUser.id)



        //Recylerview
        val adapter = MeasurementList(mMeasurementViewmodel)
        val recyclerView = view.SessionRecyclerview
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        mMeasurementViewmodel.readAllData.observe(viewLifecycleOwner, Observer { measurement ->
            adapter.setData(measurement)
        })


        view.updateFirstName_et.setText(args.currentUser.firstName)
        view.updateLastName_et.setText(args.currentUser.lastName)
        view.updateBirthday_et.setText(args.currentUser.birthday)
        view.updateSex_et.hint = args.currentUser.sex
        view.updateHandiness_et.hint = args.currentUser.handiness

        //navigate to fragment_start_pendulum
        view.startTest_btn.setOnClickListener{
            val owner = args.currentUser.id
            val action = UpdateFragmentDirections.actionUpdateFragmentToStartPendulumFragment(owner, args.currentUser)
            findNavController().navigate(action)
        }

        //navigate to fragment_update
        view.update_btn.setOnClickListener{
            updateItem()
        }

        //Add menu
        setHasOptionsMenu(true)

        return view
    }

    private fun updateItem(){
        val firstName = updateFirstName_et.text.toString()
        val lastName = updateLastName_et.text.toString()
        val birthday = updateBirthday_et.text.toString()
        val sex = updateSex_et.text.toString()
        val handiness = updateHandiness_et.text.toString()

        if(inputCheck(firstName, lastName,  sex, handiness, birthday)){
            // Create User Object
            val updatedUser = User(args.currentUser.id, firstName, lastName, birthday, sex, handiness)
            // update current User
            mUserViewModel.updateUser(updatedUser)
            Toast.makeText(requireContext(), "Updated Successfully!", Toast.LENGTH_SHORT).show()
            // Navigate Back
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }else{
            Toast.makeText(requireContext(), "Please fill out all fields!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun inputCheck(firstName: String, lastName: String, sex: String, handiness:String, age: String): Boolean{
        return !(TextUtils.isEmpty(firstName) && TextUtils.isEmpty(lastName) && TextUtils.isEmpty(sex) && TextUtils.isEmpty(handiness) && age.isEmpty())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.delete_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menu_delete){
            deleteUser()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteUser() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Yes") { _, _ ->
            mUserViewModel.deleteUser(args.currentUser)
            Toast.makeText(requireContext(), "Succesfully removed : ${args.currentUser.firstName}", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_updateFragment_to_listFragment)
        }
        builder.setNegativeButton("No") { _, _ ->}
        builder.setTitle("Delete ${args.currentUser.firstName}?")
        builder.setMessage("Are you sure you want to delete ${args.currentUser.firstName}?")
        builder.create().show()
    }
}
