package com.example.acapendulum20.fragments.pendulum

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.room.ForeignKey
import com.example.acapendulum20.R
import com.example.acapendulum20.StartPendulum
import com.example.acapendulum20.fragments.update.UpdateFragmentArgs
import com.example.acapendulum20.model.Measurement
import com.example.acapendulum20.model.User
import com.example.acapendulum20.viewmodel.MeasurementViewmodel
import com.example.acapendulum20.viewmodel.UserViewmodel
import kotlinx.android.synthetic.main.custom_row_measurement.*
import kotlinx.android.synthetic.main.fragment_add.view.*
import kotlinx.android.synthetic.main.fragment_start_pendulum.*
import kotlinx.android.synthetic.main.fragment_start_pendulum.view.*
import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class StartPendulumFragment: Fragment() {

    private val args by navArgs<StartPendulumFragmentArgs>()
    private lateinit var mMeasurementViewmodel: MeasurementViewmodel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_start_pendulum, container, false)

        mMeasurementViewmodel = ViewModelProvider(this).get(MeasurementViewmodel::class.java)

        //dropdown handiness
        val usedHand = resources.getStringArray(R.array.usedHand)
        val arrayAdapter = ArrayAdapter(requireContext(), R.layout.dropdown_item, usedHand)
        view.chooseHandiness_et.setAdapter(arrayAdapter)

        //navigate to pendulumFragment
        view.startPendulum_btn.setOnClickListener{
            startMeasurement()
        }
        return view
    }
    private fun startMeasurement() {
        val targetVelocity = (targetVelocity_et.text).toString()
        val maxAttemptTime = (maxAttemptTime_et.text).toString()
        val usedHand = (chooseHandiness_et.text).toString()
        val owner = args.owner
        val startTime = System.currentTimeMillis()

        if(inputCheck(targetVelocity, maxAttemptTime)){
            //Create Measurement Object
            val measurement = Measurement(UUID.randomUUID().toString(), targetVelocity.toFloat(),maxAttemptTime.toLong(),0.00f,0.00,startTime,0,false, usedHand,owner)
            //Add Data to Database
            mMeasurementViewmodel.addMeasurement(measurement)
            Toast.makeText(requireContext(),"Successfully added!", Toast.LENGTH_LONG).show()
            //Navigate Back
            val action = StartPendulumFragmentDirections.actionStartPendulumFragmentToPendulumFragment(measurement, args.currentUser)
            findNavController().navigate(action)
        }else{
           Toast.makeText(requireContext(),"Please fill out all fields.", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(targetVelocity: String, maxAttemptTime: String): Boolean{
        val validated = !TextUtils.isEmpty(targetVelocity) && !TextUtils.isEmpty(maxAttemptTime)
        return validated
    }
}

