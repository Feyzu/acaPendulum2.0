package com.example.acapendulum20.fragments.pendulum

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.acapendulum20.R
import com.example.acapendulum20.canvas.CanvasPendulum
import com.example.acapendulum20.model.Measurement
import com.example.acapendulum20.viewmodel.MeasurementViewmodel
import kotlinx.android.synthetic.main.fragment_pendulum.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class PendulumFragment : Fragment() {

    private val args by navArgs<PendulumFragmentArgs>()
    private lateinit var mMeasurementViewmodel: MeasurementViewmodel

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_pendulum, container, false)
        val mCanvasPendulum: CanvasPendulum = view.findViewById(R.id.canvasContainer)!!
        mMeasurementViewmodel = ViewModelProvider(this).get(MeasurementViewmodel::class.java)

        //activate the magnet
        val magnetButton = view.findViewById<Button>(R.id.activate_btn)

        //Save results
        val saveButton = view.findViewById<Button>(R.id.speichern_btn)

        saveButton.setOnClickListener(){
            saveMeasurement(false, 0.00f)
            route()
        }

        //Show targetVelocity and maxAttemptTime
        val targetVelocity = view.findViewById<TextView>(R.id.targetVelocity_view)
        val maxAttemptTime = view.findViewById<TextView>(R.id.maxAttemptTime_view)
        val timer = view.findViewById<TextView>(R.id.timer_view)
        val currentVelocity = view.findViewById<TextView>(R.id.actV_view)

        targetVelocity.text = args.measurement.targetVelocity.toString() + "cm/s"
        maxAttemptTime.text = args.measurement.maxAttemptTime.toString() + "sec"

        //time count down for "maxAttemptTime" seconds
        //with 1 second as countDown interval
        object : CountDownTimer(args.measurement.maxAttemptTime*1000, 100){
            override fun onTick(millisUntilFinished: Long) {
                val velocity = mCanvasPendulum.getVelocity()
                val time =  millisUntilFinished / 1000
                timer.setText("sec.: " + time)
                currentVelocity.setText((velocity.toString()) + "cm/s")
                if (velocity >= args.measurement.targetVelocity){
                    saveMeasurement(true, velocity.toFloat())
                    cancel()
                }
            }
            override fun onFinish() {
                timer.setText("done!")
                saveMeasurement(true, 0.00f)
            }
        }.start()

        magnetButton.setOnTouchListener(object : View.OnTouchListener{
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                if (event?.action == MotionEvent.ACTION_DOWN){
                    println("button action DOWN")
                    mCanvasPendulum.startMagnet(true)
                }

                if (event?.action == MotionEvent.ACTION_UP){
                    println("button action UP")
                    mCanvasPendulum.stopMagnet(false)
                }
                return true
            }
        })
        return view
    }

    private fun saveMeasurement(save: Boolean, velocity: Float){

        val endTime = System.currentTimeMillis()
        val maxAttemptTime = args.measurement.maxAttemptTime
        val measuredTime = ((endTime - args.measurement.startTime).toDouble()) / 1000
        //val measuredVelocity = args.measurement.measuredVelocity
        val targetVelocity = args.measurement.targetVelocity
        val usedHand = args.measurement.usedHand

        //println(measuredTime)
        if (save === true){
        val measurement = Measurement(args.measurement.id,targetVelocity,maxAttemptTime,velocity,measuredTime,args.measurement.startTime,endTime,true, usedHand,args.measurement.owner)
        mMeasurementViewmodel.finishMeasurement(measurement)
        }
    }

    private fun route(){
        val action = PendulumFragmentDirections.actionPendulumFragmentToUpdateFragment(args.currentUser)
        findNavController().navigate(action)
    }
}