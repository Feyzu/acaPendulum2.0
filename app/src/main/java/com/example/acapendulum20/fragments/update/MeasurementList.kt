package com.example.acapendulum20.fragments.list


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.recyclerview.widget.RecyclerView
import com.example.acapendulum20.R
import com.example.acapendulum20.canvas.CanvasPendulum
import com.example.acapendulum20.model.Measurement
import com.example.acapendulum20.repository.MeasurementRepository
import kotlinx.android.synthetic.main.custom_row_measurement.view.*
import java.text.SimpleDateFormat
import java.util.*
import com.example.acapendulum20.viewmodel.MeasurementViewmodel

class MeasurementList(private var mMeasurementViewmodel: MeasurementViewmodel): RecyclerView.Adapter<MeasurementList.MyViewHolder>() {



    private var sessionList = emptyList<Measurement>()

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.custom_row_measurement, parent, false))
    }

    override fun getItemCount(): Int {
        return sessionList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val currentItem = sessionList[position]

        fun convertLongToTime(time: Long): String {
            val date = Date(time)
            val format = SimpleDateFormat("dd.MM.yyyy HH:mm")
            return format.format(date)
        }
        holder.itemView.nr_txt.text = (position+1).toString()
        holder.itemView.dateStart_txt.text = "Start: " + convertLongToTime(currentItem.startTime)
        holder.itemView.targetVelocity_txt.text ="target: " + currentItem.targetVelocity.toString() + "cm/s"
        holder.itemView.velocity_txt.text ="measured: " + currentItem.measuredVelocity.toString() + "cm/s"
        holder.itemView.maxTime_txt.text ="max time: " + currentItem.maxAttemptTime.toString() + "sec"
        holder.itemView.time_txt.text ="measured time: " + currentItem.measuredTime.toString() + "sec"
        holder.itemView.usedHand_txt.text = "used hand: " + currentItem.usedHand

        holder.itemView.deleteMeasurement_btn.setOnClickListener{
            deleteMeasurement(currentItem)
        }

    }

    fun setData(measurement: List<Measurement>){
        this.sessionList = measurement
        notifyDataSetChanged()
    }

    private fun deleteMeasurement(currentItem: Measurement) {
        //mMeasurementViewmodel = ViewModelProvider(this).get(MeasurementViewmodel::class.java)
        println(mMeasurementViewmodel)
        println(currentItem)
        mMeasurementViewmodel.deleteMeasurement(currentItem)
        println("Hopefully deleted the Measurement")
    }

}

