package com.example.acapendulum20.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.acapendulum20.data.PendulumDatabase
import com.example.acapendulum20.model.Measurement
import com.example.acapendulum20.repository.MeasurementRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MeasurementViewmodel(application: Application): AndroidViewModel(application) {

    lateinit var readAllData: LiveData<List<Measurement>>
    private  val repository: MeasurementRepository

    init{
        val measurementDao = PendulumDatabase.getDatabase(application).measurementDao()
        repository = MeasurementRepository(measurementDao)
    }

    fun addMeasurement(measurement: Measurement){
        viewModelScope.launch(Dispatchers.IO){
            repository.addMeasurement(measurement)
        }
    }

    fun finishMeasurement(measurement: Measurement){
        viewModelScope.launch(Dispatchers.IO) {
            repository.finishMeasurement(measurement)
        }
    }

    fun readAllDataForUserId(id: String){
            readAllData= repository.readAllDataMeasurement(id)
    }

    fun deleteMeasurement(measurement: Measurement){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteMeasurement(measurement)
        }
    }
}
