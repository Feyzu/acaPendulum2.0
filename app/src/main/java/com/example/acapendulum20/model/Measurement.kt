package com.example.acapendulum20.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ForeignKey
import com.example.acapendulum20.data.UserDao
import com.example.acapendulum20.data.UserDao_Impl
import kotlinx.android.parcel.Parcelize
import java.time.LocalDateTime

@Parcelize
@Entity(tableName = "measurement_table")
data class Measurement(
    @PrimaryKey
    val id: String,
    val targetVelocity: Float,
    val maxAttemptTime: Long,
    val measuredVelocity: Float,
    val measuredTime: Double,
    val startTime: Long,
    val endTime: Long,
    val completed: Boolean,
    val usedHand: String,
    val owner: String,
): Parcelable

