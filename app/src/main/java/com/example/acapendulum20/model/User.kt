package com.example.acapendulum20.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "user_table")
data class User(
    @PrimaryKey
    val id: String,
    val firstName: String,
    val lastName: String,
    val birthday: String,
    val sex: String,
    val handiness: String,
): Parcelable

