package com.example.acapendulum20.repository

import androidx.lifecycle.LiveData
import com.example.acapendulum20.data.MeasurementDao
import com.example.acapendulum20.model.Measurement

class MeasurementRepository(private val measurementDao: MeasurementDao) {

    val readAllData: LiveData<List<Measurement>> = measurementDao.readAllData()

    suspend fun addMeasurement(measurement: Measurement){
        measurementDao.addMeasurement(measurement)
    }

    suspend fun finishMeasurement(measurement: Measurement){
        measurementDao.finishMeasurement(measurement)
    }

    fun readAllDataMeasurement(id: String): LiveData<List<Measurement>> {
        return measurementDao.readAllData(id)
    }

    suspend fun deleteMeasurement(measurement: Measurement){
        measurementDao.deleteMeasurement(measurement)
    }
}

