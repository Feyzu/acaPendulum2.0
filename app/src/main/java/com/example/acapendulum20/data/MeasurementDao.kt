package com.example.acapendulum20.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.acapendulum20.model.Measurement

@Dao
interface MeasurementDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE )
    fun addMeasurement(measurement: Measurement)

    @Update
    fun  finishMeasurement(measurement: Measurement)

    @Delete
    fun deleteMeasurement(measurement: Measurement)

    @Query("SELECT * FROM measurement_table WHERE owner =:id AND completed = 1")
    fun readAllData(id: String): LiveData<List<Measurement>>

    @Query("SELECT * FROM measurement_table ")
    fun readAllData(): LiveData<List<Measurement>>
}

