package com.example.acapendulum20.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.acapendulum20.model.Measurement
import com.example.acapendulum20.model.User

@Database(entities = [Measurement::class, User::class], version = 1, exportSchema = false)
abstract class PendulumDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun measurementDao(): MeasurementDao

    companion object {
        @Volatile
        private var INSTANCE: PendulumDatabase? = null

        fun getDatabase(context: Context): PendulumDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PendulumDatabase::class.java,
                    "user_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
