package com.example.acapendulum20.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.acapendulum20.model.User

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE )
    fun addUser(user: User)

    @Update
    fun  updateUser(user: User)

    @Delete
    fun deleteUser(user: User)

    @Query("DELETE FROM user_table")
    fun deleteAllUsers()

    @Query("SELECT * FROM user_table ORDER BY lastName ASC")
    fun readAllData(): LiveData<List<User>>
}
